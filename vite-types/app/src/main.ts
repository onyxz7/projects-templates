"use strict";

import "../styles/style.css";
import { setupCounterApp } from "./counter.ts";
import { toggleTheme } from "./toggle-theme.ts";
import { SvgImg } from "./svg-img.ts";

customElements.define("svg-img", SvgImg);

toggleTheme();
setupCounterApp();