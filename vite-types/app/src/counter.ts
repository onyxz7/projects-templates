"use strict";

export function setupCounterApp() {
  const appElement = document.querySelector<HTMLDivElement>("#app");
  if (appElement) {
    appElement.innerHTML = `
    <div>
      <h1>Stupid counter</h1>
      <div class="card">
        <button id="counter" type="button"></button>
      </div>
    </div>
  `;
    const counterElement =
      document.querySelector<HTMLButtonElement>("#counter");
    if (counterElement) {
      setupCounter(counterElement);
    }
  }
}

function setupCounter(element: HTMLButtonElement) {
  let counter = 0;
  const setCounter = (count: number) => {
    counter = count;
    element.innerHTML = `count is ${counter}`;
  };
  element.addEventListener("click", () => setCounter(counter + 1));
  setCounter(0);
}
