"use strict";

const toggleThemeButton: HTMLElement | null = document.getElementById(
  "toggle-theme-button"
);
const toggleThemeButtonImage: HTMLImageElement | null = document.getElementById(
  "toggle-theme-button-image"
) as HTMLImageElement | null;

export function toggleTheme(): void {
  // Initialize theme.
  const theme: string = getTheme();
  document.body.dataset.theme = theme;
  if (toggleThemeButtonImage) {
    toggleThemeButtonImage.src = `img/${theme}-mode.svg`;
  }

  // Toggle theme button.
  if (toggleThemeButton) {
    toggleThemeButton.addEventListener("click", () => {
      let theme: string = getTheme();
      theme = theme === "light" ? "dark" : "light";
      setTheme(theme);

      document.body.dataset.theme = theme;
      if (toggleThemeButtonImage) {
        toggleThemeButtonImage.src = `img/${theme}-mode.svg`;
      }
    });
  }
}

// Theme functions.
function getTheme(): string {
  return localStorage.getItem("theme") ?? "light";
}

function setTheme(theme: string): void {
  localStorage.setItem("theme", theme);
}
