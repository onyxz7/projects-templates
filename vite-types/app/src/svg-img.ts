"use strict";

export class SvgImg extends HTMLElement {
  shadow: ShadowRoot;
  static observedAttributes = ["src", "alt"];

  get src() {
    return this.getAttribute("src");
  }
  set src(value: string | null) {
    this.setAttribute("src", value ?? "");
  }

  get alt() {
    return this.getAttribute("alt");
  }
  set alt(value: string | null) {
    this.setAttribute("alt", value ?? "");
  }

  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });
  }

  attributeChangedCallback(name: string, _oldValue: string, newValue: string) {
    if (name === "src") this.updateImg(newValue);
  }

  async updateImg(src: string) {
    try {
      const response = await fetch(src);
      if (response.ok) this.shadow.innerHTML = await response.text();
    } catch (e) {
      this.shadow.innerHTML = "";
    }
  }
}
