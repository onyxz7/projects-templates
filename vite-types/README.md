# vite-types template

A template for Vite + Typescript project.
Include a automatique docker container image build.

## Deployement

You could either clone the repo and build it, or use the provided docker image.
The docker image is automaticly build in a gitlab pipeline.

### docker-compose (recommended, [click here for more info](https://docs.linuxserver.io/general/docker-compose))

```yaml
---
services:
  vite-templates:
    image: registry.gitlab.com/onyxz7/vite-templates:latest
    container_name: vite-templates
    ports:
      - 8080:80
    restart: unless-stopped
```

### docker cli ([click here for more info](https://docs.docker.com/engine/reference/commandline/cli/))

```bash
docker run -d \
  --name=vite-templates \
  -p 8080:80 \
  --restart unless-stopped \
  registry.gitlab.com/onyxz7/vite-templates:latest
```

## Parameters

Containers are configured using parameters passed at runtime (such as those above). These parameters are separated by a colon and indicate `<external>:<internal>` respectively. For example, `-p 8080:80` would expose port `80` from inside the container to be accessible from the host's IP on port `8080` outside the container.

| Parameter | Function                    |
| :-------: | --------------------------- |
| `-p 8080` | The port for the App web ui |

## Updating Info

The image is static, versioned, and require an image update and container recreation to update the app inside.

### Via Docker Compose

- Update images:

  - All images:

    ```bash
    docker-compose pull
    ```

  - Single image:

    ```bash
    docker-compose pull vite-templates
    ```

- Update containers:

  - All containers:

    ```bash
    docker-compose up -d
    ```

  - Single container:

    ```bash
    docker-compose up -d vite-templates
    ```

- You can also remove the old dangling images:

  ```bash
  docker image prune
  ```

### Via Docker Run

- Update the image:

  ```bash
  docker pull registry.gitlab.com/onyxz7/vite-templates:latest
  ```

- Stop the running container:

  ```bash
  docker stop vite-templates
  ```

- Delete the container:

  ```bash
  docker rm vite-templates
  ```

- Recreate a new container with the same docker run parameters as instructed above (if mapped correctly to a host folder, your `/config` folder and settings will be preserved)
- You can also remove the old dangling images:

  ```bash
  docker image prune
  ```

## Building locally

If you want to make local modifications to these images for development purposes or just to customize the logic:

```bash
git clone registry.gitlab.com/onyxz7/vite-templates:latest
cd vite-templates
docker build \
  --no-cache \
  --pull \
  -t registry.gitlab.com/onyxz7/vite-templates:latest .
```

## Development

Start by cloning the project, and going into

```bash
git clone git@gitlab.com:onyxz7/vite-templates.git
cd vite-templates
```

Then install the npm dependecies :

```bash
npm ci
```

### Run dev vite project

While you are developping the project, you could use the following command to run the vite project, to get live result for development:

```bash
npm run dev
```

### Preview build vite project

If you want to preview the build of the project (compile the typescript code), you could run :

```bash
npm run preview
```

### Build vite project

If you want to try to build the project (compile the typescript code), you could run :

```bash
npm run build
```

## Architecture

dist dir contain the builded app.
public contain static fils that does not need to be build, to reduce building time.
